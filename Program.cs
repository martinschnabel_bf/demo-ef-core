﻿using System.Text.Json;
using Demo;
using Microsoft.EntityFrameworkCore;

// run "dotnet ef database update" to init db

await Migrate();
await SetupData();

await using var database = new DemoContext();

PrintStatements(database);
PolymorphicQuery(database);

const string searchString = "network";
const string orderAttribute = "asset_type";
var fields = new[] {orderAttribute, "network_zone_id"};

var translatedDisplayValues = database.Set<DisplayValueTranslation>()
    .Where(v => fields.Contains(v.Name))
    .Where(dv => dv.Locale == "de-DE");
var simpleDisplayValues = database.Set<SimpleDisplayValue>().Where(v => fields.Contains(v.Name));

var germanTranslationsFilter = translatedDisplayValues
    .Where(v => v.Value.Contains(searchString))
    .Select(v => v.Asset);

var simpleDisplayValueFilter = simpleDisplayValues
    .Where(v => v.Value.Contains(searchString))
    .Select(v => v.Asset);

var assets = germanTranslationsFilter
    .Concat(simpleDisplayValueFilter)
    .Select(a => a.Id)
    .Distinct()
    .ToQueryString();

Console.WriteLine(assets);

var searchQuery = database.Set<Asset>()
    .Where(a => germanTranslationsFilter.Concat(simpleDisplayValueFilter).Contains(a))
    .Select(a => new
    {
        a.Id,
        Translations = translatedDisplayValues.Where(dv => dv.Asset == a).ToList(),
        SimpleDisplayValues = simpleDisplayValues.Where(dv => dv.Asset == a).ToList()
    })
    .Select(ap => new
    {
        ap.Id,
        ap.Translations,
        ap.SimpleDisplayValues,
        OrderAttribute = ap.Translations
            .Where(v => v.Name == orderAttribute).Select(v => v.Value)
            .Concat(ap.SimpleDisplayValues.Where(v => v.Name == orderAttribute).Select(v => v.Value))
            .First()
    })
    .OrderBy(ap => ap.OrderAttribute)
    .Select(ap => new
    {
        ap.Id,
        ap.Translations,
        ap.SimpleDisplayValues
    });

Console.WriteLine(searchQuery.ToQueryString());


searchQuery
    .ToList()
    .Select(a => MapToDto(a.Id, a.SimpleDisplayValues, a.Translations, fields))
    .ToList()
    .ForEach(ez => Console.WriteLine(JsonSerializer.Serialize(ez)));

EntryZoneChanges MapToDto(
    int assetId,
    List<SimpleDisplayValue> simpleDisplayValues,
    List<DisplayValueTranslation> displayValueTranslations,
    string[] attributeNames)
{
    var simpleDisplayValuesLookup =
        simpleDisplayValues.ToLookup(v => v.Name).ToDictionary(g => g.Key, g => g.ToArray());
    var translatedDisplayValuesLookup =
        displayValueTranslations.ToLookup(v => v.Name).ToDictionary(g => g.Key, g => g.ToArray());

    var simpleChanges = attributeNames
        .Select(n => simpleDisplayValuesLookup.TryGet(n))
        .Where(vl => vl != null)
        .Select(vl => vl!.ToLookup(v => v.Index))
        .SelectMany(vl => vl.Select(g => new AttributeValueChangeCandidate(g.First().Name, g.Key,
            g.OfType<ConfirmedSimpleDisplayValue>().Select(v => v.Value).SingleOrDefault(),
            g.OfType<LastUnConfirmedSimpleDisplayValue>().Select(v => v.Value).SingleOrDefault())));

    var translatedChanges = attributeNames
        .Select(n => translatedDisplayValuesLookup.TryGet(n))
        .Where(vl => vl != null)
        .Select(vl => vl!.ToLookup(v => v.Index))
        .SelectMany(vl => vl.Select(g => new AttributeValueChangeCandidate(
            g.First().Name,
            g.Key,
            g.OfType<ConfirmedTranslation>().Select(v => v.Value).SingleOrDefault(),
            g.OfType<LastUnconfirmedTranslation>().Select(v => v.Value).SingleOrDefault())));

    return new EntryZoneChanges(assetId, simpleChanges.Concat(translatedChanges).ToArray());
}


async Task SetupData()
{
    await using var db = new DemoContext();
    var asset = new Asset();

    db.Set<ConfirmedValue>()
        .Add(new ConfirmedValue("asset_type", 0, "phone", asset));
    db.Set<ConfirmedTranslation>()
        .Add(new ConfirmedTranslation("asset_type", 0, "Telefon", "de-DE", asset));
    db.Set<ConfirmedTranslation>()
        .Add(new ConfirmedTranslation("asset_type", 0, "phone", "en-US", asset));
    db.Set<LastUnConfirmedValue>()
        .Add(new LastUnConfirmedValue("asset_type", 0, "plc", asset));
    db.Set<LastUnconfirmedTranslation>()
        .Add(new LastUnconfirmedTranslation("asset_type", 0, "PLC", "en-US", asset));
    db.Set<LastUnconfirmedTranslation>()
        .Add(new LastUnconfirmedTranslation("asset_type", 0, "SPS", "de-DE", asset));
    db.Set<ConfirmedValue>()
        .Add(new ConfirmedValue("network_zone_id", 0, "42", asset));
    db.Set<ConfirmedSimpleDisplayValue>()
        .Add(new ConfirmedSimpleDisplayValue("network_zone_id", 0, "A network zone", asset));

    await db.SaveChangesAsync();
}

async Task Migrate()
{
    await using var demoContext = new DemoContext();

    await File.Create(demoContext.DbPath).DisposeAsync();
    await demoContext.Database.MigrateAsync();
}

void PrintStatements(DemoContext db)
{
    Console.WriteLine($"{db.Set<AttributeValue>().Count()} attribute values in data base");

    Console.WriteLine();
    Console.WriteLine("attribute value:");
    Console.WriteLine(db.Set<AttributeValue>().ToQueryString());

    Console.WriteLine();
    Console.WriteLine("translated value:");
    Console.WriteLine(db.Set<SimpleDisplayValue>().ToQueryString());

    Console.WriteLine();
    Console.WriteLine("last unconfirmed value:");
    Console.WriteLine(
        db.Set<LastUnconfirmedTranslation>()
            .Cast<IUnconfirmedValue>()
            .ToQueryString());

    Console.WriteLine();

    Console.WriteLine(
        db.Set<LastUnConfirmedValue>()
            .Cast<IUnconfirmedValue>()
            .ToQueryString());
}

void PolymorphicQuery(DemoContext db)
{
    var unconfirmedQuery = db.Set<AttributeValue>()
        .FromSqlRaw(
            "SELECT \"a\".\"Id\", \"a\".\"Discriminator\", \"a\".\"AssetId\", \"a\".\"Index\", \"a\".\"Name\", \"a\".\"Value\", \"a\".\"Locale\" FROM \"AttributeValue\" as \"a\" WHERE \"a\".\"Discriminator\" IN ('LastUnconfirmedTranslation','LastUnConfirmedValue')"
        )
        .Cast<IUnconfirmedValue>();

    Console.WriteLine(unconfirmedQuery.ToQueryString());

    var unconfirmedValues = unconfirmedQuery.ToList();

    db.Set<AttributeValue>().RemoveRange(unconfirmedValues.OfType<AttributeValue>());

    Console.WriteLine(unconfirmedValues.Count);
    Console.WriteLine(unconfirmedValues.OfType<LastUnconfirmedTranslation>().Count());
    Console.WriteLine(unconfirmedValues.OfType<LastUnConfirmedValue>().Count());
}