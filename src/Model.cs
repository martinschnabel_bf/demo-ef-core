using Microsoft.EntityFrameworkCore;

namespace Demo
{
    public class DemoContext : DbContext
    {
        public string DbPath { get; }

        public DemoContext()
        {
            const Environment.SpecialFolder folder = Environment.SpecialFolder.LocalApplicationData;
            var path = Environment.GetFolderPath(folder);
            DbPath = Path.Join(path, "demo.db");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source={DbPath}");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Asset>();

            // abstract types (only used for queries)
            modelBuilder.Entity<AttributeValue>()
                .HasOne(v => v.Asset)
                .WithMany();
            modelBuilder.Entity<SimpleDisplayValue>();
            modelBuilder.Entity<DisplayValueTranslation>();

            // unconfirmed values (original and translation)
            modelBuilder.Entity<LastUnConfirmedValue>();
            modelBuilder.Entity<LastUnconfirmedTranslation>();
            modelBuilder.Entity<LastUnConfirmedSimpleDisplayValue>();

            // confirmed values (original and translation)
            modelBuilder.Entity<ConfirmedValue>();
            modelBuilder.Entity<ConfirmedTranslation>();
            modelBuilder.Entity<ConfirmedSimpleDisplayValue>();


            base.OnModelCreating(modelBuilder);
        }
    }

    public interface IUnconfirmedValue
    {
        string Name { get; }
        int Index { get; }
    }

    public sealed class Asset
    {
        public int Id { get; set; }
    }

    public abstract record AttributeValue(string Name, int Index, string Value, Asset Asset)
    {
        public int Id { get; set; }
    }

    public sealed record ConfirmedValue(string Name, int Index, string Value, Asset Asset)
        : AttributeValue(Name, Index, Value, Asset)
    {
        public ConfirmedValue(string name, int index, string value)
            : this(name, index, value, null)
        {
        }
    }

    public sealed record LastUnConfirmedValue(string Name, int Index, string Value, Asset Asset)
        : AttributeValue(Name, Index, Value, Asset), IUnconfirmedValue
    {
        public LastUnConfirmedValue(string name, int index, string value)
            : this(name, index, value, null)
        {
        }
    }

    public abstract record SimpleDisplayValue(string Name, int Index, string Value, Asset Asset)
        : AttributeValue(Name, Index, Value, Asset);
    
    public sealed record ConfirmedSimpleDisplayValue(string Name, int Index, string Value, Asset Asset)
        : SimpleDisplayValue(Name, Index, Value, Asset)
    {
        public ConfirmedSimpleDisplayValue(string name, int index, string value)
            : this(name, index, value, null)
        {
        }
    }

    public sealed record LastUnConfirmedSimpleDisplayValue(string Name, int Index, string Value, Asset Asset)
        : SimpleDisplayValue(Name, Index, Value, Asset)
    {
        public LastUnConfirmedSimpleDisplayValue(string name, int index, string value)
            : this(name, index, value, null)
        {
        }
    }

    public abstract record DisplayValueTranslation(string Name, int Index, string Value, string Locale, Asset Asset)
        : AttributeValue(Name, Index, Value, Asset);

    public sealed record ConfirmedTranslation(string Name, int Index, string Value, string Locale, Asset Asset)
        : DisplayValueTranslation(Name, Index, Value, Locale, Asset)
    {
        public ConfirmedTranslation(string name, int index, string value, string locale)
            : this(name, index, value, locale, null)
        {
        }
    }

    public sealed record LastUnconfirmedTranslation(string Name, int Index, string Value, string Locale, Asset Asset)
        : DisplayValueTranslation(Name, Index, Value, Locale, Asset), IUnconfirmedValue
    {
        public LastUnconfirmedTranslation(string name, int index, string value, string locale)
            : this(name, index, value, locale, null)
        {
        }

        public string Locale { get; init; }
    }
}