namespace Demo;

public sealed record AttributeValueChangeCandidate(
    string AttributeName,
    int? Index,
    string? CurrentValue,
    string? NewValue);

public sealed record EntryZoneChanges(
    int AssetId,
    AttributeValueChangeCandidate[] Changes);

public static class Extensions
{
    public static T? TryGet<TKey, T>(this IDictionary<TKey, T> dictionary, TKey key) => 
        dictionary.TryGetValue(key, out var v) ? v : default;
}